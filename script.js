const SHA256 = require('crypto-js/sha256')

// async function SHA256(message) {
//     console.log("message : ",message)
//     // encode as UTF-8
//     const msgBuffer = new TextEncoder().encode(message);                    
//     console.log("msgBuffer : ",msgBuffer)
//     // hash the message
//     const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);

//     console.log("hashBuffer : ",hashBuffer)
//     // convert ArrayBuffer to Array
//     const hashArray = Array.from(new Uint8Array(hashBuffer));

//     console.log("hashArray : ",hashArray)
//     // convert bytes to hex string                  
//     const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');

//     console.log("hashHex : ",hashHex)

//     return hashHex;
// }

class Block{

    constructor(index, timestamp, data, previousHas = ''){
        this.index = index;
        this.timestamp = timestamp;
        this.data = data;
        this.previousHas = previousHas;
        this.hash = this.calculateHas();
        this.nonce = 0;
    }

    calculateHas(){
        return SHA256(this.index + this.previousHas + this.timestamp + JSON.stringify(this.data)).toString()
    }

    mineBlock(difficulty){
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){
            this.nonce++;
            this.hash = this.calculateHas()
        }
        console.log("Block mined: " + this.hash)
    }
}

class Blockchain{

    constructor(){
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 0
    }

    createGenesisBlock(){
        return new Block(0, '08/20/2022', "Genesis block", "0")
    }

    getLatestBlock(){
        return this.chain[this.chain.length - 1]
    }

    addBlock(newBlock){
        newBlock.previousHas = this.getLatestBlock().hash;
        // newBlock.hash = newBlock.calculateHas();
        newBlock.mineBlock(this.difficulty)
        this.chain.push(newBlock)
    }

    isChainValid(){

        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if(currentBlock.hash !== currentBlock.calculateHas()){
                return false
            }

            if(currentBlock.previousHas !== previousBlock.hash){
                return false
            }
        }

        return true
    }
}

let bc = new Blockchain()

console.log("Mining block 1...")
bc.addBlock(new Block(1, "08/21/2022", {amount:4}))

console.log("Mining block 2...")
bc.addBlock(new Block(2, "08/22/2022", {amount:10}))

// console.log("Is blockhain valid ? ", bc.isChainValid())

// bc.chain[1].data = {amount:100}
// bc.chain[1].hash = bc.chain[1].calculateHas()

// console.log("Is blockhain valid ? ", bc.isChainValid())

console.log(JSON.stringify(bc, null, 4))
console.log(bc)

loadCardData()
function loadCardData(){
    var cardList = '';

    for (let i = 0; i < bc.length; i++) {
        console.log(bc[i])
        cardList += '<div class="card">'+
                    '<div class="card-body">'+
                        '<h5 class="card-title">#3</h5>'+
                        '<p class="card-text">'+bc[i]+'</p>'+
                        '<a href="#" class="btn btn-primary">'+
                            '<i class="bi bi-cursor"></i>'+
                            'Details'+
                        '</a>'+
                    '</div>'+
                    '<div class="card-footer">'+
                        '<small class="text-muted">Created at 08/20/2022</small>'+
                    '</div>'+
                '</div>';   
    }
    $('#card-group-data').html(cardList);
}